# yt-dl_portable (Windows only, and kinda botched)

yt-dl was cumbersome to install for most people, so I've made a version that you just need to git clone and run

Current build is based on **testing** branch of **yt-dl**, which was almost ready for release before work on portable started,<br>
this means the release is postponed until everything works and portable and then check if it still works in normal way as well.

[yt-dl readme with pictures](https://github.com/KoleckOLP/yt-dl/blob/testing/readme.md) tiny bit outdated (picturing version 2.1.8)

## How to get it:

to install yt-dl_portable you only need git, get it here: https://git-scm.com/download/win

once you have it you have two options:
 1. run the `git clone --recurse-submodules https://gitlab.com/koleq/yt-dl_portable` command
 2. or download the installer and run install.bat from [releases](https://gitlab.com/koleq/yt-dl_portable/-/releases)

If you want, you can make a shortcut to `yt-dl_portable\yt-dl_portable.bat`<br>
and give it an icon located in `yt-dl_portable\yt-dl\gui\y.ico`
